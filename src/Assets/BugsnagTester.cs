﻿using UnityEngine;
using System.Collections;

public class BugsnagTester : MonoBehaviour {

	void Start()
	{
		Invoke("LogError", 1.0f);
	}

	void LogError () {
		Debug.LogError("A test error occured!");
	}
}

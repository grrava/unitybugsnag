using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityBugsnag;
using UnityEngine;

public class Bugsnag : MonoBehaviour {

	private static readonly System.Text.UTF8Encoding Encoding = new System.Text.UTF8Encoding();

	public string BugsnagApiKey = "";
	public string BugsnagApiUrl = "https://notify.bugsnag.com";
	public bool AutoNotify = true;
	public bool ReportInEditor = true;
	public string AppVersion = "0.0";
	public ReleaseStage ReleaseStage = ReleaseStage.Production;
	public LogSeverity NotifyLevel = LogSeverity.Exception;

	public string UserId
	{
		get { return GetValue(user, "id"); }
		set { SetValue(user, "id", value); }
	}

	public string UserName
	{
		get { return GetValue(user, "name"); }
		set { SetValue(user, "name", value); }
	}

	public string UserEmail
	{
		get { return GetValue(user, "email"); }
		set { SetValue(user, "email", value); }
	}

	public string Context { get; set; }

	private static string GetValue(JSONObject jsonObject, string key)
	{
		return jsonObject.ContainsKey(key) ? jsonObject.GetString(key) : string.Empty;
	}

	private static void SetValue(JSONObject jsonObject, string key, string value)
	{
		if (!jsonObject.ContainsKey(key))
			jsonObject.Add(key, value);
		else
			jsonObject[key] = value;
	}

	private readonly JSONObject metaData = new JSONObject();
	private readonly JSONObject user = new JSONObject();
	private readonly JSONObject device = new JSONObject();
	private readonly JSONObject notifier = new JSONObject();

	private void Awake() 
	{
		DontDestroyOnLoad(this);

		device.Add("osVersion", Environment.OSVersion.ToString());
		if(Application.isWebPlayer)
			device.Add("hostname", Application.webSecurityHostUrl);

		notifier.Add("name", "Unity");
		notifier.Add("version", "0.1.0");

		AddToTab("Unity", "Version", Application.unityVersion);
		AddToTab("Unity", "Platform", Application.platform.ToString());
		AddToTab("Unity", "System Language", Application.systemLanguage.ToString());
	}


	// create and send a notification according to https://bugsnag.com/docs/notifier-api
	private void Notify(string errorClass, string errorMessage, string stackTrace, LogSeverity severity)
	{
		var payload = new JSONObject();
		payload.Add("apiKey", BugsnagApiKey);
		payload.Add("notifier", notifier);

		var events = new JSONArray();
		payload.Add("events", events);

		var myEvent = new JSONObject();
		events.Add(myEvent);
		myEvent.Add("payloadVersion", 2);
		myEvent.Add("context", Context ?? "general");
		myEvent.Add("groupingHash", errorClass);
		myEvent.Add("severity", severity.ToString());
		myEvent.Add("user", user);
		myEvent.Add("device", device);

		var exceptions = new JSONArray();
		myEvent.Add("exceptions", exceptions);

		var jsonException = new JSONObject();
		exceptions.Add(jsonException);
		jsonException.Add("errorClass", errorClass);
		jsonException.Add("message", errorMessage);

		var jsonStackTrace = new JSONArray();
		jsonException.Add("stacktrace", jsonStackTrace);

		var lines = stackTrace.Split(new char[] {'\n'});
		foreach (var line in lines)
		{
			try
			{
				if (string.IsNullOrEmpty(line)) continue;
				var stackTraceLine = new JSONObject();
				jsonStackTrace.Add(stackTraceLine);
				if (line.Contains(" (at "))
				{
					var split = line.Split(new string[] {" (at "}, StringSplitOptions.None);
					if (split.Length < 2)
					{
						stackTraceLine.Add("file", "filename unknown");
						stackTraceLine.Add("lineNumber", 0);
						stackTraceLine.Add("method", split[0]);
					}
					else
					{
						split[1] = split[1].Substring(0, split[1].Length - 1);
						StartCoroutine(TestLog(split[1]));
						split[1] = split[1].Replace(":/", "/");
						var fileSplit = split[1].Split(new char[] {':'});
						var lineNumber = int.Parse(fileSplit[1]);

						stackTraceLine.Add("file", fileSplit[0]);
						stackTraceLine.Add("lineNumber", lineNumber);
						stackTraceLine.Add("method", split[0]);
					}
				}
				else
				{
					var split = line.Split(new string[] { ":" }, StringSplitOptions.None);
					stackTraceLine.Add("file", split[0]);
					stackTraceLine.Add("lineNumber", 0);
					stackTraceLine.Add("method", split[1]);
				}
			}
			catch (Exception)
			{
			} // if this fails, i don't care
		}

		if (jsonStackTrace.Length == 0) 
		{
			// bugsnag requires at least one entry in the stacktrace.
			var stackTraceLine = new JSONObject();
			jsonStackTrace.Add(stackTraceLine);
			stackTraceLine.Add("file", "filename unknown");
			stackTraceLine.Add("lineNumber", 0);
			stackTraceLine.Add("method", "method unknown");
		}

		var app = new JSONObject();
		myEvent.Add("app", app);
		app.Add("version", AppVersion);
		app.Add("releaseStage", ReleaseStage.ToString());

		if(metaData.Keys.Count > 0)
			myEvent.Add("metaData", metaData);
		
		StartCoroutine(SendNotification(payload));
	}

	private IEnumerator SendNotification(JSONObject payload)
	{
		var headers = new Dictionary<string, string>();
		headers.Add("Content-Type", "application/json");

		yield return 0;
		Debug.Log(payload);

		var www = new WWW(BugsnagApiUrl, Encoding.GetBytes(payload.ToString()), headers);
		yield return www;

		//Debug.Log(string.Format("www.error: {0}", www.error));
	}

	void OnEnable () {
		if (!Application.isEditor || ReportInEditor)
		{
			Debug.Log("Enabled bugsnag");
			Application.RegisterLogCallback(HandleLog);
		}
	}

	void OnDisable () {
		if (!Application.isEditor || ReportInEditor)
		{
			Application.RegisterLogCallback(null);
			Debug.Log("Disabled bugsnag");
		}
	}

	IEnumerator TestLog(string test)
	{
		yield return 0;
		Debug.Log(test);
	}
	
	void HandleLog (string logString, string stackTrace, LogType type) {
		var severity = LogSeverity.Exception;
		
		switch (type) {
		case LogType.Assert:
			severity = LogSeverity.Assert;
			break;
		case LogType.Error:
			severity = LogSeverity.Error;
			break;
		case LogType.Exception:
			severity = LogSeverity.Exception;
			break;
		case LogType.Log:
			severity = LogSeverity.Log;
			break;
		case LogType.Warning:
			severity = LogSeverity.Warning;
			break;
		}

		if(severity >= NotifyLevel && AutoNotify) {
			string errorClass, errorMessage = null;
			
			var exceptionRegEx = new Regex(@"^(?<errorClass>\S+):\s*(?<message>.*)");
			var match = exceptionRegEx.Match(logString);

			if(match.Success) {
				errorClass = match.Groups["errorClass"].Value;
				errorMessage = match.Groups["message"].Value.Trim();
			} else {
				errorClass = logString;
			}
				
			Notify(errorClass, errorMessage, stackTrace, severity);
		}
	}

	public void Notify(Exception e) {
		if(e != null) {
			if(e.StackTrace == null) {
				try {
					throw e;
				} catch(Exception ex) {
					e = ex;
				}
			}
			Notify(e.GetType().ToString(), e.Message, e.StackTrace, LogSeverity.Exception);
		}
	}

	public void AddToTab(string tabName, string attributeName, string attributeValue)
	{
		if (!metaData.ContainsKey(tabName))
			metaData.Add(tabName, new JSONObject());
		var tab = metaData.GetObject(tabName);
		if (!tab.ContainsKey(attributeName))
			tab.Add(attributeName, attributeValue);
		else
			tab[attributeName] = new JSONValue(attributeValue);
	}

	public void ClearTab(string tabName)
	{
		if(metaData.ContainsKey(tabName))
			metaData.Remove(tabName);
	}
}

namespace UnityBugsnag
{
	public enum LogSeverity
	{
		Log,
		Warning,
		Assert,
		Error,
		Exception
	}

	public enum ReleaseStage
	{
		Production,
		Staging,
		Development
	}
}